'use strict';


let seconds = 0;
let minutes= 0;
let horas= 0;
let dias= 0;

function clock() {
    const intervalId = setInterval(() => {
        seconds++;
        if (seconds > 59) {
            seconds=0;
            minutes++;
        }
        if (minutes > 59){
            minutes=0;
            horas++;
        }
        if(horas > 23){
            horas=0;
            dias++;
        }
        
    }, 1000);
}
clock();

setInterval(()=>{
    console.log(`tiempo de ejecucion: ${dias} dias, ${horas} horas, ${minutes} minutos, ${seconds} segundos`)
},5000)
