'use strict';


async function arrayDeUsuarios(numeroDeUsuarios){
    const url=`https://randomuser.me/api/?results=${numeroDeUsuarios}`
    const usuarios= []
    const respuesta= await fetch(url)
    console.log(respuesta)
    const informacion= await respuesta.json()
    console.log(informacion)
    const datos= informacion.results 
    console.log(datos)
    for (let i=0; i<= numeroDeUsuarios-1;i++){
        const usuario= {
        username: datos[i].login.username,
        name: datos[i].name.first,
        lastname: datos[i].name.last,
        gender: datos[i].gender,
        country:datos[i].location.country,
        email: datos[i].email,
        picture: datos[i].picture.large,
        }
        usuarios.push(usuario)
    }
        
    console.log(usuarios)

}
arrayDeUsuarios(5)


