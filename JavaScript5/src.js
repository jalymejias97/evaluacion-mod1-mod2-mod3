'use strict';

const respuestaGeneral = await fetch("https://rickandmortyapi.com/api/");
const informacionCompleta = await respuestaGeneral.json();
const episodios = informacionCompleta.episodes;
const respuestasEpisodios = await fetch(episodios);
const informacionCompletaEpisodios = await respuestasEpisodios.json();
const informacionEpisodio = informacionCompletaEpisodios.results;
const episodiosDeEnero = informacionEpisodio.filter((informacionEpisodio) => {
  return informacionEpisodio.air_date.startsWith("January");
});
const arraysDePersonajesEnero = episodiosDeEnero.map((episodiosDeEnero) => {
  return episodiosDeEnero.characters;
});
const listaPersonajes = [];
for (let i = 0; i < arraysDePersonajesEnero.length; i++) {
  for (let j = 0; j < arraysDePersonajesEnero[i].length; j++) {
    listaPersonajes.push(arraysDePersonajesEnero[i][j]);
  }
}
for (let i = 0; i < listaPersonajes.length; i++) {
  const personajes = await fetch(listaPersonajes[i]);
  const informacionPersonajes = await personajes.json();
  const nombrePersonajes = informacionPersonajes.name;
  console.log(nombrePersonajes);
}
