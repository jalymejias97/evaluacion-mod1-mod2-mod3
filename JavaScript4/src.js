'use strict';

/*const personas = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];*/


const personas = [
    { nombre: 'A-Jay'},
    { nombre: 'Manuel'},
    { nombre: 'Eddie'},
    { nombre: 'A-Jay'},
    { nombre: 'Su'},
    { nombre: 'Reean'},
    { nombre: 'Manuel'},
    { nombre: 'A-Jay'},
    { nombre: 'Zacharie'},
    { nombre: 'Zacharie'},
    { nombre: 'Tyra'},
    { nombre: 'Rishi'},
    { nombre: 'Arun'},
    { nombre: 'Kenton'},
];

let personasMap = personas.map(item=>{
    return [item.nombre,item]
});
  let personasMapArr = new Map(personasMap); // Pares de clave y valor

  let unicos = [...personasMapArr.values()]; // Conversión a un array

console.log(unicos);



